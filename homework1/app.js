var http = require('http'),
	config = require('./config'),
	path = require('path'),
	express = require('express'),
	routes = require('./routes'),
	app = express();


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html')
app.use('/', routes);
app.use(express.static(path.join(__dirname, 'public')));

http.createServer(app).listen(config.get('port'), function() {
	console.log('Express server listening on port ' + config.get('port'));
});