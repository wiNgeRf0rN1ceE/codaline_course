// DATABASE
// =============================================================================

// call the packages we need
var mongoose = require('mongoose'),
	config = require('../config'),
	User = require('../models/user');

//Connect to database
mongoose.connect(config.get('mongoose:uri'), config.get('mongoose:options'));
