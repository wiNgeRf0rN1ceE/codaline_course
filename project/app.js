// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express'),
	path = require('path'),
	config = require('./config'),
	db = require('./db'),
	bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser'),
	router = require('./routes/api'),
	app = express();


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'static')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

app.use('/api', router);

app.get('/', function (req, res) {
	res.render('index');
});


// START THE SERVER
// =============================================================================
app.listen(config.get('port'), function () {
	console.log('Example app listening on port 8000!');
});