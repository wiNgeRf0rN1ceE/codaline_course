var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var Users = new Schema({
    username: String,
    mail: String,
    role: { type: String, default: 'user' }
})

module.exports = mongoose.model('Users', Users);