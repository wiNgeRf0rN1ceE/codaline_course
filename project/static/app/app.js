(function () {

    angular
        .module('app', [
            'ngRoute',
            'ngResource',
            'ui.router',
            'ui.bootstrap'
        ])
        .run(function () {
            console.log('app running ...')
        });
})();